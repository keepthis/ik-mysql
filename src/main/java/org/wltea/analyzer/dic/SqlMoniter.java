package org.wltea.analyzer.dic;

import java.security.AccessController;
import java.security.PrivilegedAction;

import org.apache.logging.log4j.Logger;
import org.elasticsearch.SpecialPermission;
import org.wltea.analyzer.help.ESPluginLoggerFactory;

public class SqlMoniter implements Runnable {

	private static final Logger logger = ESPluginLoggerFactory.getLogger(SqlMoniter.class.getName());

	@Override
	public void run() {
		SpecialPermission.check();
		AccessController.doPrivileged((PrivilegedAction<Void>) () -> {
			this.runUnprivileged();
			return null;
		});
	}

	/**
	 * 监控流程：
	 */

	public void runUnprivileged() {
		logger.info("schdule start to load sqlDict............");
		Dictionary.getSingleton().reloadSqlDict();
	}

}
